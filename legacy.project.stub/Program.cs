﻿using business.framework;
using business.shared.Messages;
using System;
using System.Threading.Tasks;

namespace legacy.project.stub
{
    class Program
    {
        /// <summary>
        /// provided for reference
        /// </summary>
        public class Message
        {
            public string CustomerID { get; set; }
            public DateTime FromDateLocal { get; set; }
            public int NumberMonths { get; set; }
        }

        

        /// <summary>
        /// Legacy Stub For Testing
        /// Message is normally read from a queue or sent via an API call depending on calling service
        /// </summary>
        /// <param name="args"></param>
        static async Task Main(string[] args)
        {
            var mockMessage = new ProcessGSTMessage { CustomerID = "Cust1234", FromDateLocal = new DateTime(2019, 3, 1, 0, 0, 0, DateTimeKind.Local), NumberMonths = 3 };
            await Task.Run(async () => {
                var success = false;

                success = await ProcessGST.GetProcessGSTBasedOnDate(mockMessage.FromDateLocal).PrepareAndSendReport(mockMessage.CustomerID, mockMessage.FromDateLocal, mockMessage.NumberMonths);
            });
        }
    }
}
