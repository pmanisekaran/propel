﻿using Dapper;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace business.framework
{
    // this is abstract class because you want year specific implementation of this class to be used
    public abstract class ProcessGST : IProcessGST
    {
        private static readonly string _databaseConnection;
        private static readonly string _baseServiceEndpoint;
        private static readonly List<GSTRateAndPeriod> Rates = new List<GSTRateAndPeriod>();

        // static constructor
        static ProcessGST()
        {
            _databaseConnection = ConfigurationManager.AppSettings["DatabaseConnection"];
            _baseServiceEndpoint = ConfigurationManager.AppSettings["BaseServiceEndpoint"];

            // you can load this gst rate and period from database if you do not want to change app when gst changes next time. Change will be just DB upgrade
            // or you can load rate and period from config file itself, so there is no need for new app deployment or db upgrade, but just config

            Rates.Add(new GSTRateAndPeriod(0.1m, DateTime.MinValue, new DateTime(2019, 12, 31)));
            Rates.Add(new GSTRateAndPeriod(0.15m, new DateTime(2020, 1, 1), DateTime.MaxValue));
        }
        //normal constructor
        public ProcessGST()
        {

        }

        //get you an object of  IprocessGST that has year specific iplementation
        public static IProcessGST GetProcessGSTBasedOnDate(DateTime dateTime)
        {

            // this is implemented this way provinding means for year specific implementation 
            //if the way GST is calculated in addition just gst rate change

            if (dateTime.Year < 2020)
                return new ProcessGST2019();
            else
                return new ProcessGST2020();

        }

        public virtual async Task<bool> PrepareAndSendReport(string customerId, DateTime fromDateLocal, int numMonths)
        {
            bool sucess = false;

            try
            {
                //assumption: SP returns just one numeric column back
                Tuple<decimal, decimal> purchaseGSTData = await GetGstData(fromDateLocal, numMonths, "GetPurchaseData");
                Tuple<decimal, decimal> invoiceGSTData = await GetGstData(fromDateLocal, numMonths, "GetInvoiceData");
                var t = SendData(customerId, purchaseGSTData.Item1, purchaseGSTData.Item2, invoiceGSTData.Item1, invoiceGSTData.Item2);
                t.Wait();
                sucess = true;
            }
            catch (SqlException ex)
            {

                Console.WriteLine("something wrong in SQL :" + ex.ToString());
                // ideally you want to throw and let calling method handle it
            }
            catch (Exception ex)
            {
                //log exception somewhere
                Console.WriteLine("something wrong: " + ex.ToString());
                // ideally you want to throw and let calling method handle it

            }
            return sucess;
        }

        private async Task<Tuple<decimal, decimal>> GetGstData(DateTime fromDateLocal, int numMonths, string gstDataSP)
        {
            var total = 0m;
            var gstTotal = 0m;
            var gstRateAndPeriod = ProcessGST.Rates.First(x => x.StartDate <= fromDateLocal && x.EndDate >= fromDateLocal);
            using (var connection = new SqlConnection(_databaseConnection))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(gstDataSP, connection))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FromDate", fromDateLocal);
                    cmd.Parameters.AddWithValue("@ToDate", fromDateLocal.AddMonths(numMonths));
                    var reader = await cmd.ExecuteReaderAsync();
                    while (reader.HasRows)
                    {
                        total += Math.Round(reader.GetDecimal(0) * (1m + gstRateAndPeriod.Rate), 2);
                        gstTotal += Math.Round(reader.GetDecimal(0) * gstRateAndPeriod.Rate, 2);
                    }
                }
            }
            return new Tuple<decimal, decimal>(total, gstTotal);
        }

        private async Task SendData(string customerID, decimal purchasesTotal, decimal purchasesGstTotal,
            decimal invoicesTotal, decimal invoicesGstTotal)
        {
            using (var client = new HttpClient { BaseAddress = new Uri(_baseServiceEndpoint) })
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "0vr67gh38dj3djsh59dkp32");
                var response = await client.PostAsync("/api/receiveGstData", new StringContent($"{{ \"customerId\" : \"{customerID}\", \"purchases\" : {{ \"total\" : \"{purchasesTotal}\", \"gst\" : \"{purchasesGstTotal}\" }}, \"invoices\" : {{ \"total\" : \"{invoicesTotal}\", \"gst\" : \"{invoicesGstTotal}\" }} }}", Encoding.UTF8, "application/json"));
            }
        }
    }
}
