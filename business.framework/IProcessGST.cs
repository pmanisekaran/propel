﻿using System;
using System.Threading.Tasks;

namespace business.framework
{
	public interface IProcessGST
	{
		Task<bool> PrepareAndSendReport(string customerId, DateTime fromDateLocal, int numMonths);
	}
}