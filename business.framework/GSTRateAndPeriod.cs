﻿using System;
using System.Collections.Generic;
using System.Text;

namespace business.framework
{
	public class GSTRateAndPeriod
	{

		public GSTRateAndPeriod(decimal rate, DateTime startDate, DateTime endDate)
		{
			Rate = rate;
			StartDate = startDate;
			EndDate = endDate;
		}
		public decimal Rate { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
	}
}
