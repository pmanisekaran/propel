﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace business.framework
{
	public class ProcessGST2020 : ProcessGST
	{


		public ProcessGST2020()
		{ }
		public override Task<bool> PrepareAndSendReport(string customerId, DateTime fromDateLocal, int numMonths)
		{
			// if govt changed the way GST is calculated for 2020, then here is place to implement year specific changes

			return base.PrepareAndSendReport(customerId, fromDateLocal, numMonths);
		}
	}
}
